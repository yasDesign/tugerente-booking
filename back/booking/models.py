from django.db import models
from django.utils.translation import gettext_lazy as _

from datetime import datetime
from django.db.models import Q


# Create your models here.
class Customer(models.Model):
    nro_customer = models.IntegerField(unique=True, verbose_name="Nro Customer")
    first_name = models.CharField(max_length=150, verbose_name="First name")
    last_name = models.CharField(max_length=150, verbose_name="Last name")
    nit = models.CharField(max_length=150, verbose_name="NIT", blank=True)
    ci = models.CharField(max_length=150, verbose_name="CI", blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.ci}"


class RoomQuerySet(models.QuerySet):
    def is_available(self):
        return self.is_available_at(datetime.now())

    def is_available_date(self, prt_date: datetime):
        return self.is_available_at(prt_date)

    def is_available_at(self, prt_checkin: datetime, prt_checkout: datetime = None):
        if prt_checkout:
            assert prt_checkin < prt_checkout, "The range is invalid"
        else:
            prt_checkout = prt_checkin

        paid_booking = (Booking.objects.filter(status=Booking.State.PAID).filter(
            Q(checkin__gte=prt_checkin) | Q(checkout__lte=prt_checkout)))
        return self.exclude(bookings__in=paid_booking)


class Room(models.Model):
    nro_room = models.IntegerField(unique=True, verbose_name="Nro Room")
    description = models.TextField(blank=True, verbose_name="Descripcion")
    price = models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name="Price")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = RoomQuerySet.as_manager()

    def __str__(self):
        return f"nro :{self.nro_room} price: {self.price}"


class Booking(models.Model):
    class State(models.TextChoices):
        PENDING = "pending", _("Pending")
        PAID = "paid", _("Paid")
        REMOVED = "removed", _("Removed")

    class PaymentType(models.TextChoices):
        CASH = "cash", _("Cash")
        CREDIT_CARD = "cc", _("Credit Card")
        BANK_DEPOSIT = "bank", _("Bank Deposit")

    checkin = models.DateField(verbose_name="Date Checkin")
    checkout = models.DateField(verbose_name="Date Checkout")
    state = models.CharField(max_length=15, choices=State.choices, default=State.PENDING, verbose_name="State")
    payment_type = models.CharField(max_length=5, choices=PaymentType.choices, blank=True, null=True)
    paid_amount = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    customer = models.ForeignKey(Customer, related_name="bookings", on_delete=models.CASCADE)
    room = models.ForeignKey(Room, related_name="bookings", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.id} {self.state}"

    def save(self, **kwargs):
        if self.id:
            if self.state == Booking.State.PENDING:
                if self.paid_amount == self.room.price:
                    self.status = Booking.State.PAID
        super().save(**kwargs)
