from rest_framework import viewsets
from booking.models import Customer, Room, Booking
from .serializers import CustomerSerializer, RoomSerializer, BookingSerializer, BookingPaySerializer, \
    BookingCancelledSerializer
from rest_framework.decorators import action
from rest_framework.response import Response


class CustomerViewSet(viewsets.ModelViewSet):
    model = Customer
    serializer_class = CustomerSerializer

    def get_queryset(self):
        return Customer.objects.all()


class RoomViewSet(viewsets.ModelViewSet):
    model = Room
    serializer_class = RoomSerializer

    def get_queryset(self):
        return Room.objects.all()

    @action(methods=['get'], detail=False, serializer_class=RoomSerializer)
    def get_available(self, request, **kwargs):
        try:
            bookings = self.get_queryset().is_available()
            serializer = self.get_serializer(bookings, many=True)
            return Response(serializer.data, status=200)
        except:
            return Response({'message': 'Get Available Error'}, status=400)


class BookingViewSet(viewsets.ModelViewSet):
    model = Booking
    serializer_class = BookingSerializer

    def get_queryset(self):
        return Booking.objects.all()


    @action(methods=['post'], detail=True, serializer_class=BookingCancelledSerializer)
    def cancel(self, request, **kwargs):
        try:
            confirm = request.data['confirm']
            booking = self.get_object()
            if booking.state == Booking.State.PENDING and confirm:
                booking.state = Booking.State.CANCELED
            return Response(BookingSerializer(booking).data, status=200)
        except:
            return Response({'message': 'Cancel Error'}, status=400)


    @action(methods=['post'], detail=True, serializer_class=BookingPaySerializer)
    def pay(self, request, **kwargs):
        try:
            booking = self.get_object()
            booking.paid_amount = request.data['paid_amount']
            booking.payment_type = request.data['payment_type']
            booking.save()
            return Response(BookingSerializer(booking).data, status=200)
        except:
            return Response({'message': 'Pay Error'}, status=400)


