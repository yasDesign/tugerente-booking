from rest_framework import serializers
from booking.models import Customer, Room, Booking


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ("nro_customer", "first_name", "last_name", "nit", "ci")


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ("id", "nro_room", "price", "description", "created_at", "updated_at")


class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        fields = "id", "customer", "room", "state", "checkin", "checkout", "paid_amount", "payment_type", "created_at", "updated_at"


class BookingCancelledSerializer(serializers.Serializer):
    confirm = serializers.BooleanField(default=False)


class BookingPaySerializer(serializers.Serializer):
    paid_amount = serializers.IntegerField(required=True)
    payment_type = serializers.ChoiceField(choices=Booking.PaymentType, required=True)
