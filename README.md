# tuGerente.com - Booking
Dado los ejercicios propuesto por la empresa se analizado ambos problemas y se ha desarrollado las siguientes propuestas:

## FRONT
### Requisitos
1. Entrá a tuGerente.com y registrate en nuestra prueba gratuita de 14 días
2. Encontrarás varios módulos (VENTAS, GASTOS, RECURSOS HUMANOS, etc) entra al módulo de VENTAS
3. En este módulo encontras varias opciones (VENTAS, COTIZACIÓN, CLIENTE, etc), enfócate en la opción de VENTAS
4. Proponé un nuevo mockup de toda esta opción, OJO solo queremos el mockup de la opción VENTAS dentro del módulo de VENTAS
Eres libre de usar la herramienta que desees para hacer esta tarea.
### Solucion Propuesta
![alt text](https://gitlab.com/yasDesign/tugerente-booking/-/raw/master/front/ventas-mockup.png)
En el problema del frontend se propone
- Devido a que el campo de vision vertical de la tabla es demasiado reducido por los filtros se propone mover los filtros de los datos y filtros de columna a un drawer
- Los botones y otras opciones se propone posicionar a los extremos tanto superior como inferior para mantener una vision central mas limpia
- Como sugerencia se ha dejado un espacio central en la por encima de la tabla en caso de que se desee posicionar un titulo 

## BACK
### Requisitos
Utilizando Django REST Framework, desarrollá los endpoints para el sistema de reservas de habitación de un hotel.
CONDICIONES:
- Las reservas pueden tener 3 estados: Pendiente, Pagado y Eliminado.
- Los datos a almacenar para la reserva son: los detalles del cuarto reservado, los días de estadía, los datos de facturación e identificación del cliente, el monto pagado y el método de pago.

### Solucion propuesta
En el problema del backend se propone el lo siguiente

### Diagrama
![alt text](https://gitlab.com/yasDesign/tugerente-booking/-/raw/master/BOOKING%20diagram.jpg)

El diagrama nos muestra los siguientes modelos y relaciones
- [Customer] - Modelo que contine los datos del cliente necesarios para poder realizar una reservacion de un cuarto
- [Booking] - Modelo que se presenta como intermediario entre Customer y Room ademas de presentar datos necesarios de una facturacion como ser el monto de pago y el tipo de pago, finalmente cumple con presentar los dias de estadia mediante fecha de ingreso y fecha de salida
- [Room] - Este modelo nos muestra solo los datos mas basicos de un modelo como ser nro de cuarto y descripcion

Los modelos se ha estructurado de esa maneracon el objetivo de cumplir solamente con los requisitos

## Instalacion

1. Clonar el repositorio
    git clone git@gitlab.com:yasDesign/tugerente-booking.git
2. Acceder al projecto
    cd tugerente-booking/
    cd back/
3. Instalar los requerimientos
    pip install -r requeriments.py
4. Ejecutar la migracion y creacion de super usuario
    python manage.py migrate
    python manage.py createsuper
5. Ejecutar el projecto
    python manage.py runserver
6. Probar los API
    En el navegador ir a http://localhost:8000/api/

## Herramientas Utilizadas

Las herramientas utilizadas para el los problemas fueron:

| Plugin | README |
| ------ | ------ |
| Django rest Framework | [https://www.django-rest-framework.org/]  |
| drf-yasg | [https://drf-yasg.readthedocs.io/en/stable/index.html]  |
| wireframepro | [https://wireframepro.mockflow.com] |

## Recomendaciones para futuros cambios
- Implementar test
- Ampliar las validaciones necesarias para los campos de fecha de entrada y fecha de salida del registro de reservacion
- Implementar las validaciones necesarias como ser al intentar actualizar el estado una reservacion
- Implementar sistema de autentificacion de usuario
- Desarrollar tabla Invoice para almacenar las facturas
- Cambiar el campo type_payment del Booking a modelo
- Cambiar el campo state del del Booking a un modelo

## Observacion
 Toda la  implementacion se realizo solamente con el objetivo de cumplir los requisitos propuestos y de obtener un projecto simple y rapido.
 Los cambios que pueden realizar se deberan ser consultados de acuerdo a la necesidad y metodo de trabajar utilizada por el cliente final
 
